## Software-Testing mit GitLab CI/CD und Docker

### Ausführung von Tests in Docker
Für diese Aufgabe benötigen Sie lediglich Docker (https://docs.docker.com/desktop/). Stellen Sie sicher, dass Docker auf Ihrem System installiert und betriebsbereit ist. 
Sollten Sie bereits Java und Maven auf Ihrem System installiert haben, können Sie diese auch verwenden. In diesem Fall müssen Sie Docker nicht installiert haben.
Überprüfen Sie die beigefügte pom.xml-Datei, um sich mit den Maven-Abhängigkeiten Ihres Projekts vertraut zu machen. Verwenden Sie den folgenden Docker-Befehl, um die JUnit-Tests in einer isolierten Umgebung auszuführen:

```
docker run --rm -v "$PWD":/usr/a4 -w /usr/a4 maven:3-openjdk-18 mvn test
```
Sie könne hier `mvn test` durch beliebige andere maven Befehle ersetzen.
Dieser Befehl läd jedoch alle Abhängigkeiten bei jedem Aufruf herunter. Um dies zu vermeiden, können Sie auch
interaktiv in einem Docker-Container arbeiten. Dazu verwenden Sie den folgenden Befehl:

```
docker run --rm -v "$PWD":/usr/a4 -w /usr/a4 -it maven:3-openjdk-18 /bin/bash
```

Damit gelangen Sie in eine bash shell im Docker-Container. Dort können Sie dann die Maven-Befehle ausführen, ohne dass die Abhängigkeiten jedes Mal neu heruntergeladen werden müssen.

### Testfallerstellung für `SimpleCalculator`
Untersuchen Sie die triviale SimpleCalculator-Klasse im Verzeichnis src/main/java und entwickeln Sie JUnit-Tests, um die Unterschiede zwischen Anweisungsüberdeckung, Zweigüberdeckung und Pfadüberdeckung zu demonstrieren. 
Ihre Hauptaufgabe besteht darin, für jede dieser Testarten spezifische Testfälle zu schreiben und mit Kommentaren zu versehen, um zu illustrieren, wie sich jede Testart von den anderen unterscheidet. Beachten Sie, dass dabei bewusst Redundanzen entstehen, die helfen, die Unterschiede und Gemeinsamkeiten der Testmethoden zu verdeutlichen.

Mit dem befehl `mvn jacoco:report` können Sie nach der Ausführung der tests die Coverage-Reports generieren. Diese finden Sie im Verzeichnis `target/site/jacoco/`. JaCoCo gibt Ihnen Informationen zu Anwendungs und Zweigüberdeckung. Die Pfadüberdeckung können Sie mit dem Tool PathCov ermitteln. Dieses finden Sie im Verzeichnis `src/main/java/pathcov/`. Die Ausführung erfolgt mit dem Befehl `java -jar pathcov.jar <classfile>`. Die Classfiles finden Sie im Verzeichnis `target/classes/`.

Beispielhafte Ausführung:
```
docker run --rm -v "$PWD":/usr/a4 -w /usr/a4 -it maven:3-openjdk-18 /bin/bash

mvn test
mvn jacoco:report
```


### Testfallerstellung für `AdvancedCalculator`
Untersuchen Sie die AdvancedCalculator-Klasse im Verzeichnis src/main/java. Machen Sie sich gründlich mit den Methoden und deren Funktionsweisen vertraut. Entwickeln Sie in src/test/java JUnit-Tests für die Methoden der AdvancedCalculator-Klasse. Beachten Sie bei der Erstellung Ihrer Tests die Coding-Standards für Unittests, um die Lesbarkeit und Wartbarkeit des Codes zu gewährleisten.



Stellen Sie sicher, dass alle Tests erfolgreich durchlaufen. Überprüfen Sie die Testergebnisse sorgfältig, um sicherzustellen, dass keine Fehler oder Probleme vorliegen.

### GitLab CI/CD Pipeline
Forken Sie dieses GitLab-Repository, falls noch nicht geschehen. Konfigurieren Sie die GitLab CI/CD Pipeline in Ihrem Repository. Diese Pipeline soll bei jedem Push automatisch ein Maven Build des Projekts durchführen und alle JUnit-Tests ausführen. Achten Sie darauf, das Maven-Image in Ihrer .gitlab-ci.yml-Konfigurationsdatei zu spezifizieren. Dies stellt sicher, dass Ihre Pipeline die notwendigen Abhängigkeiten für den Build-Prozess und die Testausführung hat.

### Testen der Pipeline
Führen Sie einen Test-Commit in Ihrem GitLab-Repository durch, um die Funktionalität Ihrer CI/CD-Pipeline zu überprüfen. Beobachten Sie den Prozess der Pipeline auf der GitLab-Benutzeroberfläche. Achten Sie darauf, dass jede Phase (Stage) wie erwartet abläuft und keine Fehler auftreten.

