public class SimpleCalculator {

    public int calculate(int a, int b, boolean addFlag, boolean multiplyFlag) {
        int result;

        if (addFlag) {
            result = a + b;
        } else {
            result = a - b;
        }

        if (multiplyFlag) {
            result *= 2;
        } else {
            result /= 2;
        }

        return result;
    }
}
