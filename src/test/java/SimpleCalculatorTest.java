import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SimpleCalculatorTest {

    // Anweisungsüberdeckung (Statement Coverage)
    // Ziel: Sicherstellen, dass jede Anweisung in der Methode mindestens einmal ausgeführt wird.
    @Test
    public void testStatementCoverage() {
        SimpleCalculator calc = new SimpleCalculator();
        // TODO
    }

    // TODO: Mehr Testfälle für Anweisungsüberdeckung, Pfadüberdeckung
}
